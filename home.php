<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- font awesome -->
    <link rel="stylesheet" href="web-fonts-with-css/css/fontawesome-all.css">
    <!-- Font face -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.min.css">
    <title>Premium Spaces | Welcome</title>
  </head>
  <body class="home">
    <header role="banner">
      <a href="#maincontent" id="skiptocontent">skip to main content</a>
      <!-- home navbar -->
      <div class="bg-white d-flex align-items-start justify-content-center">
        <div class="container d-lg-flex justify-content-lg-center">
          <nav class="navbar header-nav navbar-expand-xl flex-md-fill flex-lg-unset">
            <a class="navbar-brand" href="#"><img src="img/logo.png" alt=""></a>
            <button type="button" class="btn btn-primary d-block d-xl-none ml-auto mr-3">Pay Online</button>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation"><div class="ham"></div><div class="ham"></div><div class="ham"></div></button>
            <div class="collapse navbar-collapse" id="navbarText">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Locations</a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Texas</a>
                    <a class="dropdown-item" href="#">Virginia</a>
                    <a class="dropdown-item" href="#">Louisiana</a>
                    <a class="dropdown-item" href="#">Minnesota</a>
                    <a class="dropdown-item" href="#">Missouri</a>
                    <a class="dropdown-item" href="#">Oklahoma</a>
                  </div>
                </li>
                <li class="nav-item"><a class="nav-link" href="#">Storage Tools</a></li>
                <li class="nav-item"><a class="nav-link" href="#">About</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Blog</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Contact</a></li>
                <li lass="nav-item"><button type="button" class="btn btn-primary mt-4">Pay Online</button></li>
              </ul>
            </div>
          </nav>
        </div>
        <button type="button" class="btn btn-link login d-none d-xl-block">Login <span class="ml-2 far fa-user fa-lg"></span></button>
      </div>
    </header>
    <main name="maincontent" id="maincontent" role="main">
      <div class="hero homepage-hero d-flex justify-content-center align-items-end">
        <div class="homepage-hero-search bg-white p-4">
          <div class="row align-items-center">
            <div class="col-12 col-lg-6">
              <h1 class="mb-3 mb-lg-0 text-center">Where do you need storage?</h1>
            </div>
            <div class="col-12 col-lg-6 offset-lg-0">
              <div class="text-center text-md-left"><label for="StorageSearch">Zip or City, State</label></div>
              <div class="input-group input-group-lg">
                <input type="text" id="StorageSearch" class="form-control" aria-label="Find storage search input">
                <div class="input-group-append" id="button-addon4">
                  <button class="btn btn-light-grey" type="button"><span class="fas fa-location-arrow"></span></button>
                  <button class="btn btn-primary" type="button">Search</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container mt-md-7 mt-lg-5">
        <div class="row">
          <div class="col">
            <h2 class="text-uppercase text-center fs-3 mt-5 mb-5 brand-primary-color fw-600">Storage by City</h2>
          </div>
        </div>
        <div class="row justify-content-center">
          <nav>
            <div class="nav nav-tabs city-tabs justify-content-center" id="nav-tab" role="tablist">
              <a class="nav-item nav-link btn btn-home-city-search home-texas active" id="nav-texas-tab" data-toggle="tab" href="#nav-texas" role="tab" aria-controls="nav-texas" aria-selected="true">TX</a>
              <a class="nav-item nav-link btn btn-home-city-search home-virginia" id="nav-virginia-tab" data-toggle="tab" href="#nav-virginia" role="tab" aria-controls="nav-virginia" aria-selected="false">VA</a>
              <a class="nav-item nav-link btn btn-home-city-search home-louisiana" id="nav-louisiana-tab" data-toggle="tab" href="#nav-louisiana" role="tab" aria-controls="nav-louisiana" aria-selected="false">LA</a>
              <a class="nav-item nav-link btn btn-home-city-search home-minnesota" id="nav-minnesota-tab" data-toggle="tab" href="#nav-minnesota" role="tab" aria-controls="nav-minnesota" aria-selected="false">MN</a>
              <a class="nav-item nav-link btn btn-home-city-search home-missouri" id="nav-missouri-tab" data-toggle="tab" href="#nav-missouri" role="tab" aria-controls="nav-missouri" aria-selected="false">MO</a>
              <a class="nav-item nav-link btn btn-home-city-search home-oklahoma" id="nav-oklahoma-tab" data-toggle="tab" href="#nav-oklahoma" role="tab" aria-controls="nav-oklahoma" aria-selected="false">OK</a>
            </div>
          </nav>
        </div>
        <div class="tab-content city-tab-content pb-5" id="cityTabContent">
          <div class="tab-pane fade show active" id="nav-texas" role="tabpanel" aria-labelledby="nav-texas-tab">
            <div class="row justify-content-center mt-5 mb-5"><a class="font-weight-bolder fs-4" href="#">Cities in Texas (27)</a></div>
            <div class="row city-link-row">
              <div class="col-6 col-md-3">
                <ul class="list-unstyled">
                  <li><a href="#"><span>Austin</span></a></li>
                  <li><a href="#"><span>Baytown</span></a></li>
                  <li><a href="#"><span>Channelview</span></a></li>
                  <li><a href="#"><span>Cibolo</span></a></li>
                  <li><a href="#"><span>Conroe</span></a></li>
                  <li><a href="#"><span>Cypress</span></a></li>
                  <li><a href="#"><span>Frisco</span></a></li>
                </ul>
              </div>
              <div class="col-6 col-md-3">
                <ul class="list-unstyled">
                  <li><a href="#"><span>Georgetown</span></a></li>
                  <li><a href="#"><span>Galveston</span></a></li>
                  <li><a href="#"><span>Groves</span></a></li>
                  <li><a href="#"><span>Houston</span></a></li>
                  <li><a href="#"><span>Humble</span></a></li>
                  <li><a href="#"><span>Katy</span></a></li>
                  <li><a href="#"><span>League City</span></a></li>
                </ul>
              </div>
              <div class="col-6 col-md-3">
                <ul class="list-unstyled">
                  <li><a href="#"><span>Leander</span></a></li>
                  <li><a href="#"><span>Liberty Hill</span></a></li>
                  <li><a href="#"><span>Magnolia</span></a></li>
                  <li><a href="#"><span>McKinney</span></a></li>
                  <li><a href="#"><span>Murphy</span></a></li>
                  <li><a href="#"><span>New Braunfels</span></a></li>
                  <li><a href="#"><span>Pfugerville</span></a></li>
                </ul>
              </div>
              <div class="col-6 col-md-3">
                <ul class="list-unstyled">
                  <li><a href="#"><span>Port Arthur</span></a></li>
                  <li><a href="#"><span>Richmond</span></a></li>
                  <li><a href="#"><span>Rosenberg</span></a></li>
                  <li><a href="#"><span>Rosesharon</span></a></li>
                  <li><a href="#"><span>San Antonio</span></a></li>
                  <li><a href="#"><span>Spring</span></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="nav-virginia" role="tabpanel" aria-labelledby="nav-virginia-tab">
            <div class="row justify-content-center mt-5 mb-5"><a class="font-weight-bolder fs-4" href="#">Cities in Virginia (27)</a></div>
            <div class="row city-link-row">
              <div class="col-6 col-md-3">
                <ul class="list-unstyled">
                  <li><a href="#"><span>Richmond</span></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="nav-louisiana" role="tabpanel" aria-labelledby="nav-louisiana-tab">
            <div class="row justify-content-center mt-5 mb-5"><a class="font-weight-bolder fs-4" href="#">Cities in Louisiana (27)</a></div>
            <div class="row city-link-row">
              <div class="col-6 col-md-3">
                <ul class="list-unstyled">
                  <li><a href="#"><span>Baton Rouge</span></a></li>
                  <li><a href="#"><span>Shreveport</span></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="nav-minnesota" role="tabpanel" aria-labelledby="nav-minnesota-tab">
            <div class="row justify-content-center mt-5 mb-5"><a class="font-weight-bolder fs-4" href="#">Cities in Minnesota (27)</a></div>
            <div class="row city-link-row">
              <div class="col-6 col-md-3">
                <ul class="list-unstyled">
                  <li><a href="#"><span>Blaine</span></a></li>
                  <li><a href="#"><span>Inver Grove Heights</span></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="nav-missouri" role="tabpanel" aria-labelledby="nav-missouri-tab">
            <div class="row justify-content-center mt-5 mb-5"><a class="font-weight-bolder fs-4" href="#">Cities in Missouri (27)</a></div>
            <div class="row city-link-row">
              <div class="col-6 col-md-3">
                <ul class="list-unstyled">
                  <li><a href="#"><span>Valley Park Storage</span></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="nav-oklahoma" role="tabpanel" aria-labelledby="nav-oklahoma-tab">
            <div class="row justify-content-center mt-5 mb-5"><a class="font-weight-bolder fs-4" href="#">Cities in Oklahoma (27)</a></div>
            <div class="row city-link-row">
              <div class="col-6 col-md-3">
                <ul class="list-unstyled">
                  <li><a href="#"><span>Goldsby</span></a></li>
                  <li><a href="#"><span>Moore</span></a></li>
                  <li><a href="#"><span>Norman</span></a></li>
                  <li><a href="#"><span>Oklahoma City 73117</span></a></li>
                  <li><a href="#"><span>Oklahoma City 73135</span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Storage Tools -->
      <div class="storage-tools pb-5">
        <div class="container">
          <div class="row justify-content-center">
            <h2 class="text-uppercase text-center fs-3 mt-5 mb-5 fw-600 color-white">Storage Tools</h2>
          </div>
          <div class="row justify-content-center">
            <div class="col-12 col-md-6">
              <a href="#" class="storage-toolbox d-flex mb-2 mb-md-0">
                <img class="toolbox-feature-img" src="img/home-size-guide.jpg">
                <div class="toolbox d-flex flex-column flex-fill justify-content-center align-items-center"><img class="mb-2" src="img/home-tools-ruler.png" width="30"> Size Guide</div>
                <div class="tiny-square top-left"></div><div class="tiny-square top-right"></div><div class="tiny-square bottom-left"></div><div class="tiny-square bottom-right"></div>
              </a>
            </div>
            <div class="col-12 col-md-6">
              <a href="#" class="storage-toolbox d-flex">
                <img class="toolbox-feature-img" src="img/home-storage-tips.jpg">
                <div class="toolbox d-flex flex-column flex-fill justify-content-center align-items-center"><img class="mb-2" src="img/home-tool-clipboard.png" width="30"> Storage Tips</div>
                <div class="tiny-square top-left"></div><div class="tiny-square top-right"></div><div class="tiny-square bottom-left"></div><div class="tiny-square bottom-right"></div>
              </a>
            </div>
          </div>
          <div class="row align-items-center">
            <div class="col">
              <h3 class="text-uppercase text-center fs-3 mt-5 mb-3 fw-600 color-white">Need Help?</h3>
              <p class="text-white text-center">Visit our <a class="storage-tools-link" href="/faq">FAQ</a> or <a class="storage-tools-link" href="/contact-us">contact us</a> today.</p>
            </div>
          </div>
        </div>
        <div class="square top-left"></div><div class="square top-right"></div><div class="square bg-white bottom-left"></div><div class="square bg-white bottom-right"></div>
      </div>
      <!-- Footer CTA -->
      <div class="bg-white pt-5 pb-5 footer-cta">
        <div class="container">
          <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-5">
              <h3 class="fs-4 text-md-right text-uppercase font-weight-bolder brand-primary-color mb-3mb-md-0">Rent or Reserve Today</h3>
            </div>
            <div class="col-12 col-md-6">
              <label for="StorageSearch">Zip or City, State</label>
              <div class="input-group input-group-lg">
                <input type="text" id="StorageSearch" class="form-control" aria-label="Find storage search input">
                <div class="input-group-append" id="button-addon4">
                  <button class="btn btn-light-grey" type="button"><span class="fas fa-location-arrow brand-primary-color"></span></button>
                  <button class="btn btn-primary" type="button">Search</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>